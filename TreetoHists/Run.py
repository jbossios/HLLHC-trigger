#!/usr/bin/python
import os, sys
from ROOT import *
param=sys.argv

Debug = False

TTrees = [
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ1_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ2_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ3_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ4_tree.root/", 
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.03_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ5_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ6_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/TriggerStudies/v2/user.jbossios.02_10_2018_HLLHC_JetTriggerStudies_moreInfo.JZ7_tree.root/",
]

####################################################################################
# DO NOT MODIFY
####################################################################################

command = ""
for path in TTrees:
  for File in os.listdir(path):
    # Check if root file contains a tree
    iFile = TFile.Open(path+File,"read")
    if not iFile:
      print "Error: file not found"	      
      sys.exit(0)
    tree = TTree()
    TDir = iFile.GetDirectory("TreeAlgo")
    if not TDir:
      continue # metadata file
    tree = TDir.Get("nominal")
    if not tree:
      continue
    # Run if exist the tree
    command += "TreetoHists "
    if Debug:
      command += " --debug=TRUE"
    command += " --pathUSER="
    command += path
    command += " --inputFileUSER="
    command += File
    command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)
