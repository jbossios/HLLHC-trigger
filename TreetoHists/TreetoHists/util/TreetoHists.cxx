/* **********************************************************************\
 *                                                                      *
 *  #   Name:   TreetoHists      	                                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 24/08/18 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "../TreetoHists/TreetoHists.h"

// Variables used to estimate closure
bool applyInsitu = false;
TString insituFile = "";
TString rel_histoname = "JETALGO_EtaInterCalibration";
TString PATH_user = "";
TString InputFile_user = "";

TString DirectoryName = "TreeAlgo";
TString TreeName      = "nominal";
double Rmatch         = 0.3;
double RIsoT          = 0.4 * 2.5;
double RIsoR          = 0.4 * 1.5;
double JetEtaCut      = 4.5;
int neta              = 45;

// pT bins
const int npTBins = 46;
double pTBins[npTBins+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.};

const int nEtaBins = 6;
double EtaBins[nEtaBins+1] = {0., 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

// mu bins
const int nMuBins = 8;
int muBins[nMuBins+1]  = {180,185,190,195,200,205,210,215,220};

bool ComputeSampleWeight = true;
bool m_debug      = false;

void EnableBranches();
void SetBranchAddress();
void BookHistograms();

struct SortByPt {
  template <class A, class B> bool operator() (A a, B b){
    return a.Pt() >= b.Pt();
  }
};

int main(int argc, char* argv[]) {

  gROOT->ProcessLine(".L loader.C+");
  
  std::string pathUser = "";
  std::string inputFileUser = "";
  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--pathUSER=")   != std::string::npos) pathUser = v[1];
    if ( opt.find("--inputFileUSER=")   != std::string::npos) inputFileUser = v[1];
    
    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }

    if ( opt.find("--computeSampleWeights=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) ComputeSampleWeight= true;
      if (v[1].find("FALSE") != std::string::npos) ComputeSampleWeight= false;
    }
    
  }//End: Loop over input options

  PATH_user = pathUser;
  InputFile_user = inputFileUser;

  // Protections
  if(PATH_user=="")      std::cout << "ERROR: No pathUSER specified in Run.py" << std::endl;
  if(InputFile_user=="") std::cout << "ERROR: No inputFileUSER specified in Run.py" << std::endl;

  //-----------------------
  // Getting File
  //-----------------------

  TString inputFile = PATH_user;
  inputFile += InputFile_user;
  std::cout << "Opening: " << inputFile << std::endl;
  ft = TFile::Open(inputFile.Data());
  ft->cd();
  if(ft == NULL) {
    std::cout<<"Returned null file: "<<ft<<", exiting"<<std::endl;
    return 1;
  }

  //-----------------------
  // Getting Tree
  //-----------------------

  TDirectory* TDirF_tmp = ft->GetDirectory(DirectoryName.Data());
  TDirF_tmp->GetObject(TreeName.Data(), tree);
   
  //---------------------------
  // Enable necessary branches
  //---------------------------
  EnableBranches();
  SetBranchAddress();
  // Number of events
  int entries = tree->GetEntries();
  if(m_debug) std::cout << "Total number of entries: " << entries << std::endl;
  if(m_debug) entries = 10;
  if(m_debug) std::cout << "Running over " << entries << " entries" << std::endl;

  //---------------------
  // Booking Histograms
  //---------------------
  if(m_debug) std::cout << "Booking Histograms" << std::endl;
  BookHistograms();
  if(m_debug) std::cout << "Histograms Booked" << std::endl;

  double weight = 1.;
  
  // Get pyAMI values and metadata when computing sample weights
  double xs  = 1;
  double eff = 1;
  double Denominator = 1;
  if(ComputeSampleWeight){

    if(m_debug)	std::cout << "Getting metadata and AMI information" << std::endl;

    if(PATH_user.Contains("JZ1")) JZSlice = "1";
    if(PATH_user.Contains("JZ2")) JZSlice = "2";
    if(PATH_user.Contains("JZ3")) JZSlice = "3";
    if(PATH_user.Contains("JZ4")) JZSlice = "4";
    if(PATH_user.Contains("JZ5")) JZSlice = "5";
    if(PATH_user.Contains("JZ6")) JZSlice = "6";
    if(PATH_user.Contains("JZ7")) JZSlice = "7";

    TH1D* cutflow; // Histogram with the total number of events
    TString cutflowhistoname;
    cutflowhistoname = "MetaData_EventCount";
    if(m_debug) std::cout << "Getting TH1D: " << cutflowhistoname << std::endl;
    // Open File metadata file
    TString TotalNevents1file = PATH_user;
    TotalNevents1file += "JZ"+JZSlice+"_metadata.root";
    TFile* file1events = new TFile(TotalNevents1file,"read");
    // Getting the number of events for this JZX sample
    cutflow = (TH1D*)file1events->Get(cutflowhistoname);
    Denominator = cutflow->GetBinContent(1);
    if(m_debug) std::cout << "Number of events: " << Denominator << std::endl;
    // Open File with AMI values
    std::vector<double> runNumbers;
    std::vector<double> xss;
    std::vector<double> effs;
    // Input File
    std::ifstream in;
    TString AMIfile = "../../../pyAMI/Outputs/HLLHC.txt";
    if(m_debug) std::cout << "Reading AMI values from: " << AMIfile << std::endl;
    in.open(AMIfile,std::ifstream::in);
    double t0, t1, t2;
    // Reading values
    while (in >> t0 >> t1 >> t2) {
      std::vector<double> temp;
      runNumbers.push_back(t0);
      xss.push_back(t1);
      effs.push_back(t2);
    }
    in.close();
    bool runFound = false;
    int RunNumber = 1;
    if(PATH_user.Contains("JZ1")) RunNumber = 147911;
    if(PATH_user.Contains("JZ2")) RunNumber = 147912;
    if(PATH_user.Contains("JZ3")) RunNumber = 147913;
    if(PATH_user.Contains("JZ4")) RunNumber = 147914;
    if(PATH_user.Contains("JZ5")) RunNumber = 147915;
    if(PATH_user.Contains("JZ6")) RunNumber = 147916;
    if(PATH_user.Contains("JZ7")) RunNumber = 147917;
    for(unsigned int irun=0;irun<runNumbers.size();++irun){
      if(RunNumber==runNumbers.at(irun)){
        runFound = true;
        xs  = xss.at(irun);
        eff = effs.at(irun);
      }
    }
    if(!runFound){
      std::cout << "RunNumber not found in AMI file, exiting" << std::endl;
      return 1;
    }
  } // end ComputeSampleWeight

  // OutputFile
  TString outputFile = "Results/JZ";
  outputFile += JZSlice; outputFile += "/";
  outputFile += InputFile_user;

  //------------------
  // Loop over entries
  //------------------
  bool should_skip = false;
  if(m_debug) std::cout << "Loop over entries" << std::endl;
  for (int entry=0; entry<entries; ++entry) {
    
    //---------------
    // Cleaning values
    //---------------
    
    //if(m_debug) std::cout << "Cleaning vectors" << std::endl;

    runNumber = -999;
    mcEventWeight = -999;

    // Cleaning vectors
    Jet_pt->clear();
    Jet_E->clear();
    Jet_phi->clear();
    Jet_eta->clear();
    Jet_Timing->clear();
    Jet_detectorEta->clear();
    Jet_constituent_pt->clear();
    Jet_clean_passLooseBad->clear();
    Jet_FracSamplingMax->clear();
    TruthJet_pt->clear();
    TruthJet_E->clear();
    TruthJet_phi->clear();
    TruthJet_eta->clear();
    caloCluster_pt->clear();
    caloCluster_phi->clear();
    caloCluster_eta->clear();
    caloCluster_e->clear();
    caloCluster_time->clear();
    caloCluster_LARQ->clear();
    caloCluster_TILEQ->clear();
    caloCluster_eSampling->clear();

    // Get entry
    //if(m_debug) std::cout << "Event number: " << entry << std::endl;
    //if(m_debug) std::cout << "Before GetEntry()" << std::endl;
    tree->GetEntry(entry);
    //if(m_debug) std::cout << "After GetEntry()" << std::endl;

    // Show status
    if(entry % 1000000 == 0) std::cout << "Entry = " << entry << " of " << entries << std::endl;

    // Filling Weight
    weight = xs*eff*mcEventWeight/Denominator;

    /*if(m_debug){
      std::cout << "mcEventWeight: " << mcEventWeight << std::endl;
      std::cout << "xs: " << xs << std::endl;
      std::cout << "eff: " << eff << std::endl;
      std::cout << "Denominator: " << Denominator << std::endl;
      std::cout << "mcEventWeight*SampleWeight: " << xs*eff*mcEventWeight/Denominator << std::endl;
      std::cout << "FinalWeight: " << weight << std::endl;
    }*/

    //-----------------
    // Event Cleaning
    //-----------------

    bool passJetCleaning;
    if(m_debug) std::cout << "Applying Event Jet Cleaning" << std::endl;
    for(int j=0;j<nJets;++j){ // Loop over jets
      passJetCleaning = Jet_clean_passLooseBad->at(j);
      if(passJetCleaning!=1){ // jet not passing cleaning criteria
        should_skip = true;
        break;
      }
    }
    if(m_debug) std::cout << "Checking if event should be skipped" << std::endl;
    if(should_skip) continue; // skip event due to a non clean jet

    //-----------------
    // Event Selection
    //-----------------

    // At least one jet
    if(nJets<1 || nTruthJets<1) continue; // skip event
    
    //----------------------
    // Remove pileup events
    //----------------------
    // (for lower slices this cut prevents the leading jets from being pileup jets)
    double pTavg = Jet_pt->at(0);
    if(nJets>1) pTavg = ( Jet_pt->at(0) + Jet_pt->at(1) )*0.5;
    if( TruthJet_pt->size() == 0 || (pTavg/TruthJet_pt->at(0)>1.4) ){
      continue; // skip event
    }

    if(m_debug) std::cout << "Filling mu distribution" << std::endl;
    float mu     = actualMu;
    h_mu->Fill(mu,weight);

    //---------------------------
    // Selection of Reco Jets
    //---------------------------

    RecoJets.clear();
    //if(m_debug) std::cout << "Select Reco Jets" << std::endl;
    // Loop over reco jets
    for(int ijet=0;ijet<nJets;++ijet){
      // Quality cut
      if(Jet_E->at(ijet)<0) continue; // throw away negative energy jets!
      // Eta cut
      if(fabs(Jet_eta->at(ijet)) > JetEtaCut) continue;

      int nConstit = Jet_constituent_pt->at(ijet).size();
      float pT     = Jet_pt->at(ijet);
      float eta    = Jet_eta->at(ijet);
      float phi    = Jet_phi->at(ijet);
      float E      = Jet_E->at(ijet);
      float detEta = Jet_detectorEta->at(ijet);
      float timing = Jet_Timing->at(ijet);
      int ieta     = int(fabs(detEta)*10);
      int imu      = -1.; // index of mu bin
      for(int i=0;i<nMuBins;++i){
        if(mu>=muBins[i] and mu<muBins[i+1]) imu = i;
      }
      if(imu==-1.){
        std::cout << "mu outside range, exiting" << std::endl;
	return 0;
      }

      // Fill Histograms
      h_pT->Fill(pT,weight);
      h_timing->Fill(timing,weight);
      h_timing_vs_mu->Fill(mu,timing,weight);
      h_nconstit->Fill(nConstit,weight);
      if(ieta<neta){
        h_pt_vs_npv.at(ieta)->Fill(NPV,pT,weight);
	h_pt_vs_npv_mu.at(ieta).at(imu)->Fill(NPV,pT,weight);
	h_pt_vs_njet.at(ieta)->Fill(nJets,pT,weight);
	h_pt_vs_njet_mu.at(ieta).at(imu)->Fill(nJets,pT,weight);
	h_pt_vs_nconstit.at(ieta)->Fill(nConstit,pT,weight);
        h_pt_vs_nconstit_mu.at(ieta).at(imu)->Fill(nConstit,pT,weight);
      }

      // store selected jets in a vector
      iJet jet;
      jet.SetPtEtaPhiE(pT,eta,phi,E);
      //jet.setPassLooseBad(Jet_clean_passLooseBad->at(ijet));
      jet.setDetectorEta(detEta);
      jet.setTiming(timing);
      jet.setNConstit(nConstit);
      jet.setFracSamplingMax(Jet_FracSamplingMax->at(ijet));
      RecoJets.push_back(jet);
    }//END: loop over reco jets

    //---------------------------
    // Selection of Truth Jets
    //---------------------------

    TruthJets.clear();
    //if(m_debug) std::cout << "Select Truth Jets" << std::endl;
    // Loop over truth jets
    for(int itruthjet=0;itruthjet<nTruthJets;++itruthjet){
      // Quality cut
      if(TruthJet_E->at(itruthjet)<0) continue; // throw away negative energy jets!
      // Eta cut
      if(fabs(TruthJet_eta->at(itruthjet)) > 5.0) continue;
      iJet truthjet;
      truthjet.SetPtEtaPhiE(TruthJet_pt->at(itruthjet),TruthJet_eta->at(itruthjet),TruthJet_phi->at(itruthjet),TruthJet_E->at(itruthjet));
      TruthJets.push_back(truthjet);
    }

  
    //---------------------------
    // Fill Response Histograms
    //---------------------------

    int nRecoJets  = RecoJets.size();
    int nTruthJets = TruthJets.size();

    // Loop over selected reco jets
    for(int irecojet=0;irecojet<nRecoJets;++irecojet){

     // Get clusters that match to the jet
     Clusters.clear();
     // Loop over all clusters
     nClusters = caloCluster_pt->size();
     for(int iclus=0;iclus<nClusters;++iclus){
       // construct TLorentzVector for cluster 
       iCluster clus;
       clus.SetPtEtaPhiE(caloCluster_pt->at(iclus),caloCluster_eta->at(iclus),caloCluster_phi->at(iclus),caloCluster_e->at(iclus));
       h_cluster_e->Fill(caloCluster_e->at(iclus));
       //if(m_debug) std::cout << "Before DetalR(clus,jet)" << std::endl;
       if(clus.DeltaR(RecoJets.at(irecojet)) < 0.4){ // cluster matches the jet
         if(m_debug) std::cout << "This cluster matches the jet" << std::endl;
         clus.setTime(caloCluster_time->at(iclus));
         clus.setLARQ(caloCluster_LARQ->at(iclus));
         clus.setTILEQ(caloCluster_TILEQ->at(iclus));
	 clus.setEsampling(caloCluster_eSampling->at(iclus));
	 Clusters.push_back(clus);
         double eFrac = caloCluster_e->at(iclus)/RecoJets.at(irecojet).E();
         double clusTime = caloCluster_time->at(iclus);
	 double dr = clus.DeltaR(RecoJets.at(irecojet));
	 double jetShape = eFrac*dr;
	 clus.setJetShape(jetShape);
	 if(caloCluster_TILEQ->at(iclus)>0){ //bad cluster
           h_BadTile_ClusterTime->Fill(clusTime,weight);
	   h_BadTile_eFrac->Fill(eFrac,weight);
	   h_BadTile_nConstit->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   h_BadTile_jetShape->Fill(jetShape,weight);
	 } else {
           h_GoodTile_ClusterTime->Fill(clusTime,weight);
	   h_GoodTile_eFrac->Fill(eFrac,weight);
	   h_GoodTile_nConstit->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   h_GoodTile_jetShape->Fill(jetShape,weight);
	 }
       }
     }

     int index_1sttruth    = -1;
     double dRmin_1sttruth = 1e6;
     double dRmin_2ndtruth = 1e6;
     double dRmin_reco     = 1e6;
     bool HS = true; // Hard-scatter or pile-up jet?

     // Find nearest truth jet
     for(int itruthjet=0;itruthjet<nTruthJets;++itruthjet){
       double dr = TruthJets.at(itruthjet).DeltaR(RecoJets.at(irecojet));
       if(dr < dRmin_1sttruth){
	 dRmin_1sttruth = dr;
	 index_1sttruth = itruthjet;
       }
     }

     if(index_1sttruth==-1){ // no truth jet matches this reco jet
       HS = false;
     } else { // Potential HS jet
       // Find 2nd-nearest truth jet
       for(int itruthjet=0;itruthjet<nTruthJets;++itruthjet){
         if(itruthjet==index_1sttruth) continue;
         if(TruthJets.at(itruthjet).Pt() < 7.) continue;
         double dr = TruthJets.at(itruthjet).DeltaR(RecoJets.at(irecojet));
         if(dr < dRmin_2ndtruth) dRmin_2ndtruth = dr;
       } 

       // Find nearest reco jet
       for(int jrecojet=0;jrecojet<nRecoJets;++jrecojet){
         if(jrecojet==irecojet) continue;
         if(RecoJets.at(jrecojet).Pt() < 7.) continue;
         double dr = RecoJets.at(jrecojet).DeltaR(RecoJets.at(irecojet));
         if(dr < dRmin_reco) dRmin_reco = dr;
       }
     }

     // Fill distributions for HS and PU jets
     //if(m_debug) std::cout << "Fill distributions for HS and PU jets" << std::endl;
     if(dRmin_1sttruth < Rmatch && HS){ // HS jet
       h_timing_HS->Fill(RecoJets.at(irecojet).getTiming(),weight);
       h_pT_HS->Fill(RecoJets.at(irecojet).Pt(),weight);
       h_HS_fracSamplingMax->Fill(RecoJets.at(irecojet).getFracSamplingMax(),weight);
       for(int ieta=0;ieta<nEtaBins;++ieta){
  	 if(fabs(RecoJets.at(irecojet).Eta())>EtaBins[ieta] && fabs(RecoJets.at(irecojet).Eta())<=EtaBins[ieta+1]){
	   h_HS_fracSamplingMax_vsEta.at(ieta)->Fill(RecoJets.at(irecojet).getFracSamplingMax(),weight);
	 }
       }
       if(dRmin_reco > RIsoR){ // Isolated HS jet
	 if(m_debug) std::cout << "This is a HS jet isolated" << std::endl;
	 // loop over matched clusters
	 int countNegativeEClusters = 0;
	 int nClusEfrac = 0;
	 if(m_debug) std::cout << "nMatchedClusters: " << Clusters.size() << std::endl;
	 for(unsigned int iclus=0;iclus<Clusters.size();++iclus){
	   if(m_debug) std::cout << "this cluster matches this jet" << std::endl;
           h_HS_cluster_time->Fill(Clusters.at(iclus).getTime(),weight);
           for(int ieta=0;ieta<nEtaBins;++ieta){// Loop over eta bins
  	     if(fabs(RecoJets.at(irecojet).Eta())>EtaBins[ieta] && fabs(RecoJets.at(irecojet).Eta())<=EtaBins[ieta+1]){
               h_HS_cluster_time_vsEta.at(ieta)->Fill(Clusters.at(iclus).getTime(),weight);
	     }
           }
           h_HS_cluster_e->Fill(Clusters.at(iclus).E(),weight);
	   for(int isample=0;isample<27;++isample){
	     h_HS_eFrac_sampling.at(isample)->Fill(Clusters.at(iclus).getEsampling().at(isample)/RecoJets.at(irecojet).E(),weight);
	   }
	   double eFrac = Clusters.at(iclus).E()/RecoJets.at(irecojet).E();
	   if(eFrac>0.05) nClusEfrac++;
	   h_HS_cluster_efrac->Fill(eFrac,weight);
	   double jetShape   = Clusters.at(iclus).getJetShape();
	   h_HS_clusterShape->Fill(jetShape,weight);
	   // cluster timing for clean and not clean jets
	   //if(RecoJets.at(irecojet).getPassLooseBad()) h_HS_passLooseBad_clusterTime->Fill(Clusters.at(iclus).getTime(),weight);
	   //else h_HS_notpassLooseBad_clusterTime->Fill(Clusters.at(iclus).getTime(),weight);
	   if(Clusters.at(iclus).E()<0){
	     countNegativeEClusters++;
	     //if(m_debug) std::cout << "dr(HS): " << Clusters.at(iclus).DeltaR(RecoJets.at(irecojet)) << std::endl;
	     h_HS_negativeEclusters_dr->Fill(Clusters.at(iclus).DeltaR(RecoJets.at(irecojet)));
	   }           
	 }//END: loop over matched clusters
	 h_HS_nefrac->Fill(nClusEfrac,weight);
	 //if(m_debug && countNegativeEClusters!=0) std::cout << "#NegativeEclusters (HS): " << countNegativeEClusters << std::endl;
         h_HS_negativeEclusters->Fill(countNegativeEClusters,weight);
       }
       // Loop over pT bins
       for(int ipT=0;ipT<npTBins;++ipT){
	 if(RecoJets.at(irecojet).Pt()>pTBins[ipT] && RecoJets.at(irecojet).Pt()<=pTBins[ipT+1]){
           // nConstit distributions
	   h_nconstit_HS.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
           h_HS_fracSamplingMax_vspT.at(ipT)->Fill(RecoJets.at(irecojet).getFracSamplingMax());
	   if(irecojet==0) h_nconstit_leadingHS.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   else if(irecojet==1) h_nconstit_secondHS.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   if(dRmin_reco > RIsoR){ // isolated
	     h_nconstit_isolatedHS.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   } else { // non-isolated jet
	     h_nconstit_nonisolatedHS.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   }
	 }
       }
     } else { // PU jet
       h_timing_PU->Fill(RecoJets.at(irecojet).getTiming(),weight);
       h_pT_PU->Fill(RecoJets.at(irecojet).Pt(),weight);
       h_PU_fracSamplingMax->Fill(RecoJets.at(irecojet).getFracSamplingMax());
       for(int ieta=0;ieta<nEtaBins;++ieta){
  	 if(fabs(RecoJets.at(irecojet).Eta())>EtaBins[ieta] && fabs(RecoJets.at(irecojet).Eta())<=EtaBins[ieta+1]){
	   h_PU_fracSamplingMax_vsEta.at(ieta)->Fill(RecoJets.at(irecojet).getFracSamplingMax(),weight);
	 }
       }
       if(dRmin_reco > RIsoR){ // Isolated PU jet
	 // loop over matched clusters
	 int countNegativeEClusters = 0;
         int nClusEfrac = 0;
	 for(unsigned int iclus=0;iclus<Clusters.size();++iclus){
           h_PU_cluster_time->Fill(Clusters.at(iclus).getTime(),weight);
           for(int ieta=0;ieta<nEtaBins;++ieta){// Loop over eta bins
  	     if(fabs(RecoJets.at(irecojet).Eta())>EtaBins[ieta] && fabs(RecoJets.at(irecojet).Eta())<=EtaBins[ieta+1]){
               h_PU_cluster_time_vsEta.at(ieta)->Fill(Clusters.at(iclus).getTime(),weight);
	     }
           }
           h_PU_cluster_e->Fill(Clusters.at(iclus).E(),weight);
	   for(int isample=0;isample<27;++isample){
	     h_PU_eFrac_sampling.at(isample)->Fill(Clusters.at(iclus).getEsampling().at(isample)/RecoJets.at(irecojet).E(),weight);
	   }
	   double eFrac = Clusters.at(iclus).E()/RecoJets.at(irecojet).E();
	   if(eFrac>0.05) nClusEfrac++;
	   h_PU_cluster_efrac->Fill(eFrac,weight);
	   double jetShape   = Clusters.at(iclus).getJetShape();
	   h_PU_clusterShape->Fill(jetShape,weight);
	   if(Clusters.at(iclus).E()<0){
	     countNegativeEClusters++;
	     //if(m_debug) std::cout << "dr(PU): " << Clusters.at(iclus).DeltaR(RecoJets.at(irecojet)) << std::endl;
	     h_PU_negativeEclusters_dr->Fill(Clusters.at(iclus).DeltaR(RecoJets.at(irecojet)));
           }
	 }
	 h_PU_nefrac->Fill(nClusEfrac,weight);
	 //if(m_debug && countNegativeEClusters!=0) std::cout << "#NegativeEclusters (PU): " << countNegativeEClusters << std::endl;
         h_PU_negativeEclusters->Fill(countNegativeEClusters,weight);
       }
       // Loop over pT bins
       for(int ipT=0;ipT<npTBins;++ipT){
	 if(RecoJets.at(irecojet).Pt()>pTBins[ipT] && RecoJets.at(irecojet).Pt()<=pTBins[ipT+1]){
           h_PU_fracSamplingMax_vspT.at(ipT)->Fill(RecoJets.at(irecojet).getFracSamplingMax());
           // nConstit distributions
	   h_nconstit_PU.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   if(irecojet==0) h_nconstit_leadingPU.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   else if(irecojet==1) h_nconstit_secondPU.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   if(dRmin_reco > RIsoR){ // isolated
	     h_nconstit_isolatedPU.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   } else { // non-isolated jet
	     h_nconstit_nonisolatedPU.at(ipT)->Fill(RecoJets.at(irecojet).getNConstit(),weight);
	   }
	 }
       }
     }

     //----------------------
     // Requirements
     // 1) reco-truth matching
     // 2) second nearest truth jets far enough of the reco jet (isolation)
     // 3) next reco jet far enough from the considered reco jet (isolation)
     if(!(dRmin_1sttruth < Rmatch && dRmin_2ndtruth > RIsoT && dRmin_reco > RIsoR)) continue; // Skip reco jet

     // Bins in eta detector
     int ieta = int(fabs(RecoJets.at(irecojet).getDetectorEta())*10);
     if(ieta >= neta) continue;

     // Filling response histograms
     double response = RecoJets.at(irecojet).Pt()/TruthJets.at(index_1sttruth).Pt();
     double pttruth  = TruthJets.at(index_1sttruth).Pt();
     h_resp_vs_pttruth.at(ieta)->Fill(pttruth, response, weight);
     h_resp_vs_timing.at(ieta)->Fill(RecoJets.at(irecojet).getTiming(), response, weight);
     h_resp_vs_nconstit.at(ieta)->Fill(RecoJets.at(irecojet).getNConstit(), response, weight);
     h_pttruth.at(ieta)->Fill(pttruth, pttruth); // to be able to obtain <pTtruth>
    
    }//END: loop over reco jets
        
  } // End: Loop over entries

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  if(m_debug) std::cout << "Saving hists into a ROOT file..." << std::endl;

  // Opening output file
  tout = new TFile(outputFile,"recreate");
  if(m_debug) std::cout << "output file: " << outputFile << std::endl;
  tout->cd();

  // Writing histograms
  for(int ieta=0;ieta<neta;++ieta){
    h_resp_vs_pttruth.at(ieta)->Write();
    h_resp_vs_timing.at(ieta)->Write();
    h_resp_vs_nconstit.at(ieta)->Write();
    h_pt_vs_njet.at(ieta)->Write();
    h_pt_vs_npv.at(ieta)->Write();
    h_pt_vs_nconstit.at(ieta)->Write();
    h_pttruth.at(ieta)->Write();
    for(int imu=0;imu<nMuBins;++imu){
      h_pt_vs_njet_mu.at(ieta).at(imu)->Write();
      h_pt_vs_npv_mu.at(ieta).at(imu)->Write();
      h_pt_vs_nconstit_mu.at(ieta).at(imu)->Write();
    }
  }

  for(int isample=0;isample<27;++isample){
    h_HS_eFrac_sampling.at(isample)->Write();
    h_PU_eFrac_sampling.at(isample)->Write();
  }

  for(int ieta=0;ieta<nEtaBins;++ieta){
    h_HS_fracSamplingMax_vsEta.at(ieta)->Write();
    h_PU_fracSamplingMax_vsEta.at(ieta)->Write();
    h_HS_cluster_time_vsEta.at(ieta)->Write();
    h_PU_cluster_time_vsEta.at(ieta)->Write();
  }

  for(int ipT=0;ipT<npTBins;++ipT){
    h_nconstit_HS.at(ipT)->Write();
    h_nconstit_isolatedHS.at(ipT)->Write();
    h_nconstit_nonisolatedHS.at(ipT)->Write();
    h_nconstit_leadingHS.at(ipT)->Write();
    h_nconstit_secondHS.at(ipT)->Write();
    h_nconstit_PU.at(ipT)->Write();
    h_nconstit_isolatedPU.at(ipT)->Write();
    h_nconstit_nonisolatedPU.at(ipT)->Write();
    h_nconstit_leadingPU.at(ipT)->Write();
    h_nconstit_secondPU.at(ipT)->Write();
    h_HS_fracSamplingMax_vspT.at(ipT)->Write();
    h_PU_fracSamplingMax_vspT.at(ipT)->Write();
  }
  h_pT->Write();
  h_pT_HS->Write();
  h_pT_PU->Write();
  h_timing->Write();
  h_timing_PU->Write();
  h_timing_HS->Write();
  h_timing_vs_mu->Write();
  h_nconstit->Write();
  h_mu->Write();
  h_cluster_e->Write();
  h_HS_cluster_time->Write();
  h_HS_cluster_e->Write();
  h_PU_cluster_time->Write();
  h_PU_cluster_e->Write();
  h_HS_cluster_efrac->Write();
  h_PU_cluster_efrac->Write();
  h_HS_negativeEclusters->Write();
  h_PU_negativeEclusters->Write();
  h_HS_negativeEclusters_dr->Write();
  h_PU_negativeEclusters_dr->Write();
  //h_HS_passLooseBad_clusterTime->Write();
  //h_HS_notpassLooseBad_clusterTime->Write();
  h_HS_nefrac->Write();
  h_PU_nefrac->Write();
  h_GoodTile_ClusterTime->Write();
  h_BadTile_ClusterTime->Write();
  h_GoodTile_eFrac->Write();
  h_BadTile_eFrac->Write();
  h_GoodTile_nConstit->Write();
  h_BadTile_nConstit->Write();
  h_GoodTile_jetShape->Write();
  h_BadTile_jetShape->Write();
  h_HS_clusterShape->Write();
  h_PU_clusterShape->Write();
  h_HS_fracSamplingMax->Write();
  h_PU_fracSamplingMax->Write();
 
  tout->Close();
  
  ft->Close();
    
  std::cout << ">>>>>> Done! <<<<<<" << std::endl;

  return 0;

}




/*
 *
 *
 * Personal Functions
 *
 *
 *
 * */


void EnableBranches(){

  tree->SetBranchStatus("*",0); //disable all branches
  tree->SetBranchStatus("mcChannelNumber",1);
  tree->SetBranchStatus("mcEventWeight",1);
  tree->SetBranchStatus("actualInteractionsPerCrossing",1);
  tree->SetBranchStatus("NPV",1);
  tree->SetBranchStatus("njet",1);
  tree->SetBranchStatus("jet_pt",1);
  tree->SetBranchStatus("jet_eta",1);
  tree->SetBranchStatus("jet_phi",1);
  tree->SetBranchStatus("jet_E",1);
  tree->SetBranchStatus("jet_detectorEta",1);
  tree->SetBranchStatus("jet_Timing",1);
  tree->SetBranchStatus("jet_constituent_pt",1);
  tree->SetBranchStatus("jet_clean_passLooseBad",1);
  tree->SetBranchStatus("jet_FracSamplingMax",1);
  tree->SetBranchStatus("ntruthJet",1);
  tree->SetBranchStatus("truthJet_pt",1);
  tree->SetBranchStatus("truthJet_eta",1);
  tree->SetBranchStatus("truthJet_phi",1);
  tree->SetBranchStatus("truthJet_E",1);
  tree->SetBranchStatus("caloCluster_pt",1);
  tree->SetBranchStatus("caloCluster_eta",1);
  tree->SetBranchStatus("caloCluster_phi",1);
  tree->SetBranchStatus("caloCluster_e",1);
  tree->SetBranchStatus("caloCluster_time",1);
  tree->SetBranchStatus("caloCluster_LARQ",1);
  tree->SetBranchStatus("caloCluster_TILEQ",1);
  tree->SetBranchStatus("caloCluster_eSampling",1);

}//END: EnableBranches()

void SetBranchAddress(){
  // Event
  tree->SetBranchAddress("mcChannelNumber",&runNumber);
  tree->SetBranchAddress("mcEventWeight",&mcEventWeight);
  tree->SetBranchAddress("actualInteractionsPerCrossing",&actualMu);
  tree->SetBranchAddress("NPV",&NPV);
  // Reco Jets
  tree->SetBranchAddress("njet",&nJets);
  tree->SetBranchAddress("jet_pt",&Jet_pt);
  tree->SetBranchAddress("jet_eta",&Jet_eta);
  tree->SetBranchAddress("jet_E",&Jet_E);
  tree->SetBranchAddress("jet_phi",&Jet_phi);
  tree->SetBranchAddress("jet_detectorEta",&Jet_detectorEta);
  tree->SetBranchAddress("jet_clean_passLooseBad",&Jet_clean_passLooseBad);
  tree->SetBranchAddress("jet_constituent_pt",&Jet_constituent_pt);
  tree->SetBranchAddress("jet_Timing",&Jet_Timing);
  tree->SetBranchAddress("jet_phi",&Jet_phi);
  tree->SetBranchAddress("jet_FracSamplingMax",&Jet_FracSamplingMax);
  // Truth Jets
  tree->SetBranchAddress("ntruthJet",&nTruthJets);
  tree->SetBranchAddress("truthJet_pt",&TruthJet_pt);
  tree->SetBranchAddress("truthJet_eta",&TruthJet_eta);
  tree->SetBranchAddress("truthJet_E",&TruthJet_E);
  tree->SetBranchAddress("truthJet_phi",&TruthJet_phi);
  // Clusters
  tree->SetBranchAddress("caloCluster_pt",&caloCluster_pt);
  tree->SetBranchAddress("caloCluster_eta",&caloCluster_eta);
  tree->SetBranchAddress("caloCluster_phi",&caloCluster_phi);
  tree->SetBranchAddress("caloCluster_e",&caloCluster_e);
  tree->SetBranchAddress("caloCluster_time",&caloCluster_time);
  tree->SetBranchAddress("caloCluster_LARQ",&caloCluster_LARQ);
  tree->SetBranchAddress("caloCluster_TILEQ",&caloCluster_TILEQ);
  tree->SetBranchAddress("caloCluster_eSampling",&caloCluster_eSampling);

}//END: SetBranchAddress()

void BookHistograms(){

  TH2F* htemp;
  TH1D* htemp1D;
  TString name = "";
  for(int ieta=0;ieta<neta;++ieta){
    // Response vs Timing
    name  = "h_resp_vs_pttruth_eta_";
    name        += ieta;
    htemp = new TH2F(name,name,npTBins,pTBins,300,0,3.0);
    htemp->Sumw2();
    h_resp_vs_pttruth.push_back(htemp);
    // Response vs Timing
    name  = "h_resp_vs_timing_eta_";
    name += ieta;
    htemp = new TH2F(name,name,400,-200,200,300,0,3.0);
    htemp->Sumw2();
    h_resp_vs_timing.push_back(htemp);
    // Response vs nConstit
    name  = "h_resp_vs_nconstit_eta_";
    name += ieta;
    htemp = new TH2F(name,name,200,0,100,300,0,3.0);
    htemp->Sumw2();
    h_resp_vs_nconstit.push_back(htemp);
    // Truth pT
    name  = "h_pttruth_eta_";
    name += ieta;
    htemp1D = new TH1D(name,name,npTBins,pTBins);
    htemp1D->Sumw2();
    h_pttruth.push_back(htemp1D);
    // pT vs NPV
    name  = "h_pt_vs_npv_eta_";
    name += ieta;
    htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
    htemp->Sumw2();
    h_pt_vs_npv.push_back(htemp);
    // pT vs Njet
    name  = "h_pt_vs_njet_eta_";
    name += ieta;
    htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
    htemp->Sumw2();
    h_pt_vs_njet.push_back(htemp);
    // pT vs Njet
    name  = "h_pt_vs_nconstit_eta_";
    name += ieta;
    htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
    htemp->Sumw2();
    h_pt_vs_nconstit.push_back(htemp);
    std::vector<TH2F*> vhtemp_1;
    std::vector<TH2F*> vhtemp_2;
    std::vector<TH2F*> vhtemp_3;
    for(int imu=0;imu<nMuBins;++imu){
      // pT vs NPV
      name  = "h_pt_vs_npv_eta_";
      name += ieta;
      name += "_mu_";
      name += imu;
      htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
      htemp->Sumw2();
      vhtemp_1.push_back(htemp);
      // pT vs Njet
      name  = "h_pt_vs_njet_eta_";
      name += ieta;
      name += "_mu_";
      name += imu;
      htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
      htemp->Sumw2();
      vhtemp_2.push_back(htemp);
      // pT vs Njet
      name  = "h_pt_vs_nconstit_eta_";
      name += ieta;
      name += "_mu_";
      name += imu;
      htemp = new TH2F(name,name,npTBins,pTBins,100,0,100);
      htemp->Sumw2();
      vhtemp_3.push_back(htemp);
    }//END: loop over mu bins
    h_pt_vs_npv_mu.push_back(vhtemp_1);
    h_pt_vs_njet_mu.push_back(vhtemp_2);
    h_pt_vs_nconstit_mu.push_back(vhtemp_3);
  }//END: loop over eta bins

  for(int ieta=0;ieta<nEtaBins;++ieta){
    name   = "h_HS_fracSamplingMax_eta_";
    name  += ieta;
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_HS_fracSamplingMax_vsEta.push_back(htemp1D);
    name   = "h_PU_fracSamplingMax_eta_";
    name  += ieta;
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_PU_fracSamplingMax_vsEta.push_back(htemp1D);
    name   = "h_HS_cluster_time_eta_";
    name  += ieta;
    htemp1D = new TH1D(name,"",400,-200,200);
    htemp1D->Sumw2();
    h_HS_cluster_time_vsEta.push_back(htemp1D);
    name   = "h_PU_cluster_time_eta_";
    name  += ieta;
    htemp1D = new TH1D(name,"",400,-200,200);
    htemp1D->Sumw2();
    h_PU_cluster_time_vsEta.push_back(htemp1D);
  }

  for(int isample=0;isample<27;++isample){
    name   = "h_HS_eFrac_sample_";
    name  += isample;
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_HS_eFrac_sampling.push_back(htemp1D);
    name   = "h_PU_eFrac_sample_";
    name  += isample;
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_PU_eFrac_sampling.push_back(htemp1D);
  }

  // nConstituent HS vs PU
  for(int ipT=0;ipT<npTBins;++ipT){
    name   = "nConstit_HS_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_HS.push_back(htemp1D);
    name   = "nConstit_isolatedHS_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_isolatedHS.push_back(htemp1D);
    name   = "nConstit_nonisolatedHS_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_nonisolatedHS.push_back(htemp1D);
    name   = "nConstit_leadingHS_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_leadingHS.push_back(htemp1D);
    name   = "nConstit_secondHS_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_secondHS.push_back(htemp1D);
    name   = "nConstit_PU_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_PU.push_back(htemp1D);
    name   = "nConstit_isolatedPU_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_isolatedPU.push_back(htemp1D);
    name   = "nConstit_nonisolatedPU_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_nonisolatedPU.push_back(htemp1D);
    name   = "nConstit_leadingPU_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_leadingPU.push_back(htemp1D);
    name   = "nConstit_secondPU_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",200,0,100);
    htemp1D->Sumw2();
    h_nconstit_secondPU.push_back(htemp1D);
    name   = "HS_fracSamplingMax_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_HS_fracSamplingMax_vspT.push_back(htemp1D);
    name   = "PU_fracSamplingMax_pt_";
    name  += pTBins[ipT];
    htemp1D = new TH1D(name,"",100,0,1.0);
    htemp1D->Sumw2();
    h_PU_fracSamplingMax_vspT.push_back(htemp1D);
  }

  // pTreco
  h_pT = new TH1D("pT","",npTBins,pTBins);
  h_pT->Sumw2();
  h_pT_HS = new TH1D("pT_HS","",npTBins,pTBins);
  h_pT_HS->Sumw2();
  h_pT_PU = new TH1D("pT_PU","",npTBins,pTBins);
  h_pT_PU->Sumw2();
  // Timing
  h_timing = new TH1D("Timing","",400,-200,200);
  h_timing->Sumw2();
  h_timing_PU = new TH1D("Timing_PU","",400,-200,200);
  h_timing_PU->Sumw2();
  h_timing_HS = new TH1D("Timing_HS","",400,-200,200);
  h_timing_HS->Sumw2();
  // Timing vs mu
  h_timing_vs_mu = new TH2F("h_timing_vs_mu","h_timing_vs_mu",20,180,220,400,-200,200);
  h_timing_vs_mu->Sumw2();
  // Clusters
  h_HS_cluster_time = new TH1D("h_HS_cluster_time","",400,-200,200);
  h_HS_cluster_time->Sumw2();
  h_PU_cluster_time = new TH1D("h_PU_cluster_time","",400,-200,200);
  h_PU_cluster_time->Sumw2();
  h_cluster_e = new TH1D("h_cluster_e","",2000,-1000.,1000.);
  h_cluster_e->Sumw2();
  h_HS_cluster_e = new TH1D("h_HS_cluster_e","",2000,-1000.,1000.);
  h_HS_cluster_e->Sumw2();
  h_PU_cluster_e = new TH1D("h_PU_cluster_e","",2000,-1000.,1000.);
  h_PU_cluster_e->Sumw2();
  h_HS_cluster_efrac = new TH1D("h_HS_cluster_efrac","",100,0,1.0);
  h_HS_cluster_efrac->Sumw2();
  h_PU_cluster_efrac = new TH1D("h_PU_cluster_efrac","",100,0,1.0);
  h_PU_cluster_efrac->Sumw2();
  h_HS_negativeEclusters = new TH1D("h_HS_negativeEclusters","",100,0,100);
  h_HS_negativeEclusters->Sumw2();
  h_PU_negativeEclusters = new TH1D("h_PU_negativeEclusters","",100,0,100);
  h_PU_negativeEclusters->Sumw2();
  h_HS_negativeEclusters_dr = new TH1D("h_HS_negativeEclusters_dr","",40,0,0.4);
  h_HS_negativeEclusters_dr->Sumw2();
  h_PU_negativeEclusters_dr = new TH1D("h_PU_negativeEclusters_dr","",40,0,0.4);
  h_PU_negativeEclusters_dr->Sumw2();
  //h_HS_passLooseBad_clusterTime = new TH1D("h_HS_passLooseBad_clusterTime","",400,-200,200);
  //h_HS_passLooseBad_clusterTime->Sumw2();
  //h_HS_notpassLooseBad_clusterTime = new TH1D("h_HS_notpassLooseBad_clusterTime","",400,-200,200);
  //h_HS_notpassLooseBad_clusterTime->Sumw2();
  h_HS_nefrac = new TH1D("h_HS_nefrac","",50,0,50);
  h_HS_nefrac->Sumw2();
  h_PU_nefrac = new TH1D("h_PU_nefrac","",50,0,50);
  h_PU_nefrac->Sumw2();
  h_GoodTile_ClusterTime = new TH1D("h_GoodTile_ClusterTime","",400,-200,200);
  h_GoodTile_ClusterTime->Sumw2();
  h_BadTile_ClusterTime  = new TH1D("h_BadTile_ClusterTime","",400,-200,200);
  h_BadTile_ClusterTime->Sumw2();
  h_GoodTile_eFrac       = new TH1D("h_GoodTile_eFrac","",100,0,1.0);
  h_GoodTile_eFrac->Sumw2();
  h_BadTile_eFrac        = new TH1D("h_BadTile_eFrac","",100,0,1.0);
  h_BadTile_eFrac->Sumw2();
  h_GoodTile_nConstit    = new TH1D("h_GoodTile_nConstit","",200,0,100);
  h_GoodTile_nConstit->Sumw2();
  h_BadTile_nConstit     = new TH1D("h_BadTile_nConstit","",200,0,100);
  h_BadTile_nConstit->Sumw2();
  h_GoodTile_jetShape    = new TH1D("h_GoodTile_jetShape","",100,0,0.4);
  h_GoodTile_jetShape->Sumw2();
  h_BadTile_jetShape     = new TH1D("h_BadTile_jetShape","",100,0,0.4);
  h_BadTile_jetShape->Sumw2();
  h_HS_clusterShape      = new TH1D("h_HS_clusterShape","",100,0,0.4);
  h_HS_clusterShape->Sumw2();
  h_PU_clusterShape      = new TH1D("h_PU_clusterShape","",100,0,0.4);
  h_PU_clusterShape->Sumw2();
  h_HS_fracSamplingMax   = new TH1D("h_HS_fracSamplingMax","",100,0,1.0);
  h_HS_fracSamplingMax->Sumw2();
  h_PU_fracSamplingMax   = new TH1D("h_PU_fracSamplingMax","",100,0,1.0);
  h_PU_fracSamplingMax->Sumw2();

  // nConstituents
  h_nconstit = new TH1D("nConstit","",200,0,100);
  h_nconstit->Sumw2();
  // mu
  h_mu = new TH1D("mu","",300,0,300);
  h_mu->Sumw2();

}//END: BookHistograms()

