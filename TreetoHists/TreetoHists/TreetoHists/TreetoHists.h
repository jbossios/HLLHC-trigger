#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

class iJet : public TLorentzVector {
  private:
    float detectorEta;
    float timing;
    float nconstit;
    float fracSamplingMax;
    int   passLooseBad;
  public:
    inline float  getDetectorEta(){return detectorEta;}
    inline void   setDetectorEta(double eta){detectorEta = eta;}
    inline float  getTiming(){return timing;}
    inline void   setTiming(float time){timing = time;}
    inline float  getNConstit(){return nconstit;}
    inline void   setNConstit(float n){nconstit = n;}
    inline int  getPassLooseBad(){return passLooseBad;}
    inline void   setPassLooseBad(int pass){passLooseBad = pass;}
    inline float  getFracSamplingMax(){return fracSamplingMax;}
    inline void   setFracSamplingMax(float frac){fracSamplingMax = frac;}
};

class iCluster : public TLorentzVector {
  private:
    float Time;
    float LARQ;
    float TILEQ;
    double jetShape;
    std::vector<float> eSampling;
  public:
    inline float  getTime(){return Time;}
    inline void   setTime(float time){Time = time;}
    inline float  getLARQ(){return LARQ;}
    inline void   setLARQ(float larq){LARQ = larq;}
    inline float  getTILEQ(){return TILEQ;}
    inline void   setTILEQ(float tileq){TILEQ = tileq;}
    inline std::vector<float>  getEsampling(){return eSampling;}
    inline void   setEsampling(std::vector<float> esampling){eSampling = esampling;}
    inline double getJetShape(){return jetShape;}
    inline void   setJetShape(double jetshape){jetShape = jetshape;}
};


TString PATH;
TString DataoutputFile;
TString MCinputFile;
TString MCoutputFile;
TString JZSlice;
TString JetCollection;
TString version;

// Input tree
TTree *tree;
TFile *ft;
TFile *tout;

//------------
// Histograms
//------------

std::vector<TH2F*> h_resp_vs_pttruth;
std::vector<TH2F*> h_resp_vs_timing;
std::vector<TH2F*> h_resp_vs_nconstit;
std::vector<TH2F*> h_pt_vs_njet;
std::vector<TH2F*> h_pt_vs_npv;
std::vector<TH2F*> h_pt_vs_nconstit;
std::vector<TH1D*> h_pttruth;
std::vector<TH1D*> h_nconstit_HS;
std::vector<TH1D*> h_nconstit_isolatedHS;
std::vector<TH1D*> h_nconstit_nonisolatedHS;
std::vector<TH1D*> h_nconstit_leadingHS;
std::vector<TH1D*> h_nconstit_secondHS;
std::vector<TH1D*> h_nconstit_PU;
std::vector<TH1D*> h_nconstit_isolatedPU;
std::vector<TH1D*> h_nconstit_nonisolatedPU;
std::vector<TH1D*> h_nconstit_leadingPU;
std::vector<TH1D*> h_nconstit_secondPU;
std::vector<TH1D*> h_HS_fracSamplingMax_vspT;
std::vector<TH1D*> h_PU_fracSamplingMax_vspT;
std::vector<TH1D*> h_HS_fracSamplingMax_vsEta;
std::vector<TH1D*> h_PU_fracSamplingMax_vsEta;
std::vector<TH1D*> h_HS_cluster_time_vsEta;
std::vector<TH1D*> h_PU_cluster_time_vsEta;
std::vector<TH1D*> h_HS_eFrac_sampling;
std::vector<TH1D*> h_PU_eFrac_sampling;
std::vector< std::vector<TH2F*> > h_pt_vs_njet_mu;
std::vector< std::vector<TH2F*> > h_pt_vs_npv_mu;
std::vector< std::vector<TH2F*> > h_pt_vs_nconstit_mu;
TH1D* h_pT;
TH1D* h_pT_HS;
TH1D* h_pT_PU;
TH1D* h_timing;
TH1D* h_timing_PU;
TH1D* h_timing_HS;
TH2F* h_timing_vs_mu;
TH1D* h_nconstit;
TH1D* h_mu;
TH1D* h_cluster_e;
TH1D* h_HS_cluster_time;
TH1D* h_HS_cluster_e;
TH1D* h_PU_cluster_time;
TH1D* h_PU_cluster_e;
TH1D* h_HS_cluster_efrac;
TH1D* h_PU_cluster_efrac;
TH1D* h_HS_negativeEclusters;
TH1D* h_PU_negativeEclusters;
TH1D* h_HS_negativeEclusters_dr;
TH1D* h_PU_negativeEclusters_dr;
TH1D* h_HS_passLooseBad_clusterTime;
TH1D* h_HS_notpassLooseBad_clusterTime;
TH1D* h_HS_nefrac;
TH1D* h_PU_nefrac;
TH1D* h_GoodTile_ClusterTime; 
TH1D* h_BadTile_ClusterTime;
TH1D* h_GoodTile_eFrac; 
TH1D* h_BadTile_eFrac;
TH1D* h_GoodTile_nConstit;
TH1D* h_BadTile_nConstit;
TH1D* h_GoodTile_jetShape;
TH1D* h_BadTile_jetShape;
TH1D* h_HS_clusterShape;
TH1D* h_PU_clusterShape;
TH1D* h_HS_fracSamplingMax;
TH1D* h_PU_fracSamplingMax;
  
struct prop {
    TLorentzVector Sel_Jet;
    bool           Used;
    bool           Iso;
    double         DetectorEta;
    double         JVT;
} ;
std::vector<prop> Selected_RscanJets;  
std::vector<prop> Selected_RefJets;  

struct prop_sel {
    TLorentzVector Sel_JetRef;
    TLorentzVector Sel_JetRscan;
    double         dR;
    double         detEta_Ref;  
    double         detEta_Rscan;  
} ;
std::vector<prop_sel> Selected_matched_Jets;

double Radius;
int ptmin; 

std::vector<iJet> RecoJets;
std::vector<iJet> TruthJets;
std::vector<iCluster> Clusters;

//-----------
// Variables
//-----------

// Event Info
int runNumber;
float mcEventWeight;
int NPV;
float actualMu;
float wgt;

// Reco Jets
int nJets; 
std::vector<float> *Jet_pt          = 0;
std::vector<float> *Jet_phi         = 0;
std::vector<float> *Jet_eta         = 0;
std::vector<float> *Jet_E           = 0;
std::vector<float> *Jet_detectorEta = 0;
std::vector<float> *Jet_Timing      = 0;
std::vector<float> *Jet_FracSamplingMax  = 0;
std::vector<int> *Jet_clean_passLooseBad = 0;
std::vector< std::vector<float> > *Jet_constituent_pt = 0;

// Truth Jets
int nTruthJets;
std::vector<float> *TruthJet_pt  = 0;
std::vector<float> *TruthJet_eta = 0;
std::vector<float> *TruthJet_phi = 0;
std::vector<float> *TruthJet_E   = 0;

// Clusters
int nClusters; 
std::vector<float> *caloCluster_pt    = 0;
std::vector<float> *caloCluster_phi   = 0;
std::vector<float> *caloCluster_eta   = 0;
std::vector<float> *caloCluster_e     = 0;
std::vector<float> *caloCluster_time  = 0;
std::vector<float> *caloCluster_LARQ  = 0;
std::vector<float> *caloCluster_TILEQ = 0;
std::vector<std::vector<float> > *caloCluster_eSampling = 0;

  
