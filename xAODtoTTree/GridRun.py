#!/usr/bin/python

import os, sys

param=sys.argv

# Date
LocalPath = "/afs/cern.ch/work/j/jbossios/public/HLLHC/xAODtoTTree_TriggerStudies/v_1"
date = "03_10_2018_HLLHC_JetTriggerStudies_moreInfo"
user = "jbossios"

list=[
#    "JZ1",
#    "JZ2",
#    "JZ3",
#    "JZ4",
    "JZ5",
#    "JZ6",
#    "JZ7",
]

##################################################################################
# DO NOT MODIFY
##################################################################################

for JZ in list:
  # Setting the correct sample
  if JZ == "JZ0":
    sample = "mc15_14TeV:mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ1":
    sample = "mc15_14TeV:mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ2":
    sample = "mc15_14TeV:mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ3":
    sample = "mc15_14TeV:mc15_14TeV.147913.Pythia8_AU2CT10_jetjet_JZ3W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ4":
    sample = "mc15_14TeV:mc15_14TeV.147914.Pythia8_AU2CT10_jetjet_JZ4W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ5":
    sample = "mc15_14TeV:mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ6":
    sample = "mc15_14TeV:mc15_14TeV.147916.Pythia8_AU2CT10_jetjet_JZ6W.recon.AOD.e2403_s3142_s3143_r9589"
  if JZ == "JZ7":
    sample = "mc15_14TeV:mc15_14TeV.147917.Pythia8_AU2CT10_jetjet_JZ7W.recon.AOD.e2403_s3142_s3143_r9589"

  command = "xAH_run.py "

  # Input dataset
  command += "--files "
  command += sample

  command += " --submitDir="
  command += "Outputs_HLLHCPythia_"
  command += JZ

  # Config
  command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_HLLHC.json"

  # Prun options
  #command += " --inputRucio prun --optGridNFilesPerJob=40.0 --optRemoveSubmitDir 1"
  command += " --inputRucio prun --optGridNFilesPerJob=40.0 --optRemoveSubmitDir 1"
  command += " --optGridExcludedSite=TAIWAN-LCG2_DATADISK"

  # Output dataset
  command += " --optGridOutputSampleName=user."+user+"."
  command += date
  command += "."
  command += JZ

  # Logfiles
  command += " > logfile_HLLHCPythia_"
  command += JZ
  command += " 2> errors_HLLHCPythia_"
  command += JZ

  command += " &"

  print command
  os.system(command)



