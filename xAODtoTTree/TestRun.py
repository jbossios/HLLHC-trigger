#!/usr/bin/python

import os, sys

param=sys.argv

##################################################################################
# Edit
##################################################################################

HLLHCSample = "PATH/FILE.root"
PATH = "PATH"

##################################################################################
# DO NOT MODIFY
##################################################################################

command = "xAH_run.py --files "+HLLHCSample+" --config " + PATH + "/xAODAnaHelpers/data/xah_run_HLLHC.json --submitDir outputTreesHLLHC direct > Logs/log_HLLHC 2> Logs/err_HLLHC &"

print command
os.system(command)

