#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

IJXS = True

CMEs = ["14","27"]
PDFs = ["CT14nlo"]
Ana  = ["IJXS","DIJETS"]
Bins = ["00_05","05_10","10_15","15_20","20_25","25_30"]
IJXSLegends   = ["|y|<0.5 (#times 10^{0})","0.5#leq|y|<1.0 (#times 10^{-3})","1.0#leq|y|<1.5 (#times 10^{-6})","1.5#leq|y|<2.0 (#times 10^{-9})","2.0#leq|y|<2.5 (#times 10^{-12})","2.5#leq|y|<3.0 (#times 10^{-15})"]
DIJETSLegends = ["y*<0.5 (#times 10^{0})","0.5#leq y*<1.0 (#times 10^{-3})","1.0#leq y*<1.5 (#times 10^{-6})","1.5#leq y*<2.0 (#times 10^{-9})","2.0#leq y*<2.5 (#times 10^{-12})","2.5#leq y*<3.0 (#times 10^{-15})"]
ScaleFactors  = [1,1E-3,1E-6,1E-9,1E-12,1E-15]

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Load easyPlot
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")

for ana in Ana:
  for CME in CMEs:
    print("Running "+ana+" "+CME+" TeV")
    # Plotter Instance for fomatting
    Format = Plotter1D()   # This is the constructor for format only
    Format.SetComparisonType("Normal")   # Will show second panel with ratio
    Format.ShowCME(CME)
    Format.SetLogx()
    Format.SetLogy()
    Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
    if ana=="IJXS":
      if CME=="14":
        Format.SetRange("Y",10E-26,10E10)
      if CME=="27":
        Format.SetRange("Y",10E-26,10E10)
      Format.SetAxisTitles("#it{p}_{T} [GeV]","d^{2}\sigma/d#it{p}_{T}dy [pb/GeV]") # x-axis, y-axis
    elif ana=="DIJETS":
      if CME=="14":
        Format.SetRange("Y",10E-31,10E9)
      if CME=="27":
        Format.SetRange("Y",10E-31,10E11)
      Format.SetAxisTitles("#it{m}_{jj} [GeV]","d^{2}\sigma/d#it{m}_{jj}dy* [pb/GeV]") # x-axis, y-axis
    Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
    Format.SetLineWidthAll(2)
    Format.SetMoreLogLabelsX()
    Format.SetDrawOptionAll("HIST][")
    #Format.SetTLegendPosition(0.64,0.65,0.93,0.92)
    Format.SetTLegendPosition(0.66,0.67,0.93,0.92)

    # Output PDF
    OutPDF = "../Plots/Pavel"
    if "IJXS" in ana:
      OutPDF += "_IJXS_"
    elif "DIJETS" in ana:
      OutPDF += "_DIJETS_"
    OutPDF += CME
    OutPDF += "TeV"
 
    PATH  = "/home/jbossios/cern/HLLHC/Inputs/Pavel/"
    PATH += CME
    PATH += "TeV/uncert/"

    FileNames = []
    for bin in Bins:
      if ana=="IJXS":
        if CME=="14":
          if bin!="25_30":
            FileNames.append(PATH+"incljets_rapidity_"+bin+"_R04_P2_standTheoryUncert.root")
          else:
            FileNames.append(PATH+"incljets_rapidity_"+bin+"_R04_P2_standTheoryUncert_Final.root")
        elif CME=="27":
          FileNames.append(PATH+"incljets_rapidity_"+bin+"_R04_P2_standTheoryUncert.root")
      elif ana=="DIJETS":
        FileNames.append(PATH+"dijet_HTystar_"+bin+"_R04_P2_ScaleMass_standTheoryUncert_Final.root")

    # Final Plot
    Plot = Plotter1D(OutPDF) # Name of the output file
    Plot.ApplyFormat(Format)
    counter = 0
    for file in FileNames:
      if ana=="DIJETS" and CME=="27":
        Plot.SetRange("X",300,1000000)
      if ana=="DIJETS":
        Plot.AddHist(str(counter),file,CME+str(counter))
      else:
        if CME=="14" and counter==5:
            Plot.AddHist(str(counter),file,"IJXS14TeV")
        else:
            Plot.AddHist(str(counter),file,"CT14nlo/scales/xsec")
      if ana=="IJXS":
        Plot.SetLegend(str(counter),IJXSLegends[counter])
      elif ana=="DIJETS":
        Plot.SetLegend(str(counter),DIJETSLegends[counter])
      Plot.Scale(str(counter),ScaleFactors[counter])
      counter += 1
       

    #Plot.AddText(0.50,0.88,"CT14 PDF")
    Plot.AddText(0.2,0.2,"NLOJET++ (CT14 PDF)")
    Plot.AddText(0.2,0.25,"anti-k_{t} #it{R}=0.4")
    Plot.Write()  # Saves plot into OutPDF

print ">>>> DONE <<<<"


