#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Load easyPlot
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")

# Plotter Instance for fomatting
Format = Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
Format.SetLogx()
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetAxisTitles("#it{p}_{T} [GeV]","Relative uncertainty") # x-axis, y-axis
Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
Format.SetRange("X",100,3936)
# y-axis range
#Format.SetRange("Y",-0.2,0.2)
Format.SetLineWidthAll(2)
Format.SetDrawOptionAll("HIST][")
Format.SetMoreLogLabelsX()

####################################################################################
# DO NOT MODIFY
####################################################################################

OutPDF = "../Plots/CompareResolution_MC15_vs_HLLHC_IJXS"

# Final Plot
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.AddHist("MC15c","../Inputs/Resolution_vs_pTtruth_MC15c.root","Resolution_vs_pTtruth_Eta_0")
Plot.AddHist("HLLHC","../Inputs/Resolution_vs_pTtruth_HLLHC.root","Resolution_vs_pTtruth_Eta_0")
Plot.SetColor("MC15c",kBlack)
Plot.SetColor("MC15c",kBlue)
Plot.SetLineStyle("MC15c",1)
Plot.SetLineStyle("HLLHC",2)
Plot.Write()  # Saves plot into OutPDF

print ">>>> DONE <<<<"


