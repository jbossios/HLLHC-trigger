#! /bin/bash
mkdir build
setupATLAS
cd source
lsetup "asetup AnalysisBase,21.2.20"
cd ../build
cmake ../source
make
source ${CMTCONFIG}/setup.sh
cd ..
