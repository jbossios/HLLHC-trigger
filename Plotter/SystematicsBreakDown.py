#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

IJXS   = True # DIJETS if False
Smooth = False

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")
Format = Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
Format.ShowCME("13")    # Show 13 TeV
Format.SetOutputFolder("../Plots")
#Format.AddText(0.35,0.825,"|#eta|<3.0") # x, y, text
Format.SetLogx()
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
if IJXS:
  Format.SetAxisTitles("#it{p}_{T} [GeV]","Relative uncertainty") # x-axis, y-axis
else:
  Format.SetAxisTitles("m_{jj} [GeV]","Relative uncertainty") # x-axis, y-axis
#Format.SetLegendPosition("TopLeft")  # Other options: Top, TopLeft, Bottom
Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
if IJXS:
  Format.SetRange("Y",-0.14,0.37)
else:
  Format.SetRange("Y",-0.4,0.7)
if IJXS:
  Format.SetRange("X",100,2499)
else:
  Format.SetRange("X",100,7500)
Format.SetLineWidthAll(2)
#Format.SetLineStyle("3",2)
#Format.SetLineStyle("4",2)
if Smooth:
  Format.SmoothAll()
Format.SetDrawOptionAll("HIST][")
Format.SetMoreLogLabelsX()

YMin = -0.3
YMax = 0.4

MC15c = False
HLLHC_optimistic   = False
HLLHC_conservative = False
HLLHC_pesimistic   = True
Debug = False # More verbosity and run over only 10 events

Colors = [8,8,kRed+2,kRed+2,kGreen+2,kGreen+2,kMagenta+1,kMagenta+1,kOrange+2,kOrange+2,kAzure+10,kAzure+10,kBlue+2,kBlue+2]

systs_MC15c_1 = [
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up", # DONE
  "JET_EffectiveNP_2__1down", # DONE
  "JET_EffectiveNP_3__1up", # DONE
  "JET_EffectiveNP_3__1down", # DONE
  "JET_EffectiveNP_4__1up", # DONE
  "JET_EffectiveNP_4__1down", # DONE
  "JET_EffectiveNP_5__1up", # DONE
  "JET_EffectiveNP_5__1down", # DONE
  "JET_EffectiveNP_6__1up", # DONE
  "JET_EffectiveNP_6__1down", # DONE
  "JET_EffectiveNP_7__1up", # DONE
  "JET_EffectiveNP_7__1down", # DONE
]

systs_MC15c_2 = [
  "JET_EffectiveNP_8restTerm__1up", # DONE
  "JET_EffectiveNP_8restTerm__1down", # DONE
  "JET_EtaIntercalibration_Modelling__1up", # DONE
  "JET_EtaIntercalibration_Modelling__1down", # DONE
  "JET_EtaIntercalibration_NonClosure__1up", # DONE
  "JET_EtaIntercalibration_NonClosure__1down", # DONE
  "JET_EtaIntercalibration_TotalStat__1up", # DONE
  "JET_EtaIntercalibration_TotalStat__1down", # DONE
  "JET_Flavor_Composition__1up", # DONE
  "JET_Flavor_Composition__1down", # DONE
  "JET_Flavor_Response__1up", # DONE
  "JET_Flavor_Response__1down", # DONE
]

systs_MC15c_3 = [
  "JET_Pileup_OffsetMu__1up", # DONE
  "JET_Pileup_OffsetMu__1down", # DONE
  "JET_Pileup_OffsetNPV__1up", # DONE
  "JET_Pileup_OffsetNPV__1down", # DONE
  "JET_Pileup_PtTerm__1up", # DONE
  "JET_Pileup_PtTerm__1down", # DONE
  "JET_Pileup_RhoTopology__1up", # DONE
  "JET_Pileup_RhoTopology__1down", # DONE
  "JET_PunchThrough_MC15__1up", # DONE
  "JET_PunchThrough_MC15__1down", # DONE
  "JET_SingleParticle_HighPt__1up", # DONE
  "JET_SingleParticle_HighPt__1down", # DONE
  "JET_JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_optimistic_1=[ # HLLHC optimistic
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
]

systs_HLLHC_optimistic_2=[ # HLLHC optimistic
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
]

systs_HLLHC_optimistic_3=[ # HLLHC optimistic
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]


systs_HLLHC_conservative_1=[ # HLLHC conservative
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
]

systs_HLLHC_conservative_2=[ # HLLHC conservative
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_EtaIntercalibration_Modelling__1up",
  "JET_EtaIntercalibration_Modelling__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
]

systs_HLLHC_conservative_3=[ # HLLHC conservative
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]

nSigma = 0.5

####################################################################################
# DO NOT MODIFY
####################################################################################

# Protection
if HLLHC_optimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_conservative and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_optimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_pesimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_pesimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_pesimistic and HLLHC_optimistic:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if not HLLHC_optimistic and not HLLHC_conservative and not HLLHC_pesimistic and not MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)

Systs_1 = []
Systs_2 = []
Systs_3 = []
if HLLHC_conservative or HLLHC_pesimistic:
  Systs_1 = systs_HLLHC_conservative_1
  Systs_2 = systs_HLLHC_conservative_2
  Systs_3 = systs_HLLHC_conservative_3
elif HLLHC_optimistic:
  Systs_1 = systs_HLLHC_optimistic_1
  Systs_2 = systs_HLLHC_optimistic_2
  Systs_3 = systs_HLLHC_optimistic_3
elif MC15c:
  Systs_1 = systs_MC15c_1
  Systs_2 = systs_MC15c_2
  Systs_3 = systs_MC15c_3

InputFileName = "../Inputs/"
if IJXS:
  InputFileName += "IJXS_"
else:
  InputFileName += "DIJETS_"
if HLLHC_conservative:
  InputFileName += "HLLHC_conservative"
if HLLHC_pesimistic:
  InputFileName += "HLLHC_pesimistic"
elif HLLHC_optimistic:
  InputFileName += "HLLHC_optimistic"
elif MC15c:
  InputFileName += "MC15c"
InputFileName += ".root"
InputFile = TFile.Open(InputFileName)
if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)


# Get Total up and down systematics
npTavgBins = 46
pTavgBins = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.]

# mjj binning
nMassBins = 44
massBins = [ 0., 30., 70., 110., 160., 210., 260., 310., 370., 440., 510., 590., 670., 760., 850., 950., 1060., 1180., 1310., 1450., 1600., 1760., 1940., 2120., 2330., 2550., 2780., 3040., 3310., 3610., 3930., 4270., 4640., 5040., 5470., 5940., 6440., 7000., 7630., 8317., 9066., 9882., 10771., 11740., 12797.]

if IJXS:
  hTotal_up = TH1D("Total","",npTavgBins,array('d',pTavgBins))
else:
  hTotal_up = TH1D("Total","",nMassBins,array('d',massBins))
# Loop over pT bins
nBins = npTavgBins
if not IJXS:
  nBins = nMassBins
for bin in range(1,nBins+1):
  TotalUncertainty = 0;
  for syst in Systs_1:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_2:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_3:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  TotalUncertainty = TMath.Sqrt(TotalUncertainty)
  hTotal_up.SetBinContent(bin,TotalUncertainty)
  hTotal_up.SetBinError(bin,0)
if IJXS:
  hTotal_down = TH1D("Total_down","",npTavgBins,array('d',pTavgBins))
else:
  hTotal_down = TH1D("Total_down","",nMassBins,array('d',massBins))
# Loop over pT bins
nBins = npTavgBins
if not IJXS:
  nBins = nMassBins
for bin in range(1,nBins+1):
  TotalUncertainty = 0
  for syst in Systs_1:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_2:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_3:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
    if "SINGLE" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print syst+" not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  TotalUncertainty = TMath.Sqrt(TotalUncertainty)
  hTotal_down.SetBinContent(bin,TotalUncertainty)
  hTotal_down.SetBinError(bin,0)
hTotal_down.Scale(-1)

# First Set
OutPDF = ""
if IJXS:
  OutPDF += "IJXS_"
else:
  OutPDF += "DIJETS_"
File   = "../Inputs/"
if IJXS:
  File += "IJXS_"
else:
  File += "DIJETS_"
if MC15c:
  OutPDF += "MC15c"
  Text = "MC15c"
  File   += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
  File   += "HLLHC_conservative"
  Text = "HL-LHC (conservative)"
if HLLHC_pesimistic:
  OutPDF += "HLLHC_pesimistic"
  File   += "HLLHC_pesimistic"
  Text = "HL-LHC (pesimistic)"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
  File   += "HLLHC_optimistic"
  Text = "HL-LHC (optimistic)"
OutPDF += "_1"
File += ".root"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.AddText(0.2,0.765,Text) # x, y, text
Plot.AddText(0.35,0.82,Text) # x, y, text
if not MC15c:
  Plot.ShowCME("14")    # Show 13 TeV
counter = 0
for syst in Systs_1:
  Legend = TString(syst)
  Legend.ReplaceAll("__1up","")
  Legend.ReplaceAll("JET_","")
  Plot.AddHist(str(counter),File,syst) # ID,TFile,histName
  Plot.SetLegend(str(counter),Legend) # ID,TFile,histName
  Plot.SetColor(str(counter),Colors[counter])
  if not counter % 2 == 0:
    Plot.NoTLegend(str(counter))
  counter += 1
Plot.AddHist("Total",hTotal_up)
Plot.AddHist("TotalDown",hTotal_down,)
Plot.SetColor("Total",kBlack)
Plot.SetColor("TotalDown",kBlack)
Plot.NoTLegend("TotalDown")
Plot.Write()  # Saves plot into "pT.pdf"

# Second Set
OutPDF = ""
if IJXS:
  OutPDF += "IJXS_"
else:
  OutPDF += "DIJETS_"
if MC15c:
  OutPDF += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
if HLLHC_pesimistic:
  OutPDF += "HLLHC_pesimistic"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
OutPDF += "_2"
Plot2 = Plotter1D(OutPDF) # Name of the output file
Plot2.ApplyFormat(Format)
Plot2.SetLegendPosition("TopLeft")  # Other options: Top, TopLeft, Bottom
if MC15c:
  Plot2.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
else:
  Plot2.ShowCME("14")    # Show 13 TeV
#Plot2.AddText(0.2,0.765,Text) # x, y, text
Plot2.AddText(0.35,0.82,Text) # x, y, text
counter = 0
for syst in Systs_2:
  Legend = TString(syst)
  Legend.ReplaceAll("__1up","")
  Legend.ReplaceAll("JET_","")
  Plot2.AddHist(str(counter),File,syst) # ID,TFile,histName
  Plot2.SetLegend(str(counter),Legend) # ID,TFile,histName
  Plot2.SetColor(str(counter),Colors[counter])
  if not counter % 2 == 0:
    Plot2.NoTLegend(str(counter))
  counter += 1
Plot2.AddHist("Total",hTotal_up)
Plot2.AddHist("TotalDown",hTotal_down,)
Plot2.SetColor("Total",kBlack)
Plot2.SetColor("TotalDown",kBlack)
Plot2.NoTLegend("TotalDown")
if IJXS:
  Plot2.SetRange("Y",-0.15,0.3)
if MC15c:
  Plot2.SetTLegendPosition(0.425+0.075,0.589,0.92+0.075,0.925)
Plot2.Write()  # Saves plot into "pT.pdf"

# Third Set
OutPDF = ""
if IJXS:
  OutPDF += "IJXS_"
else:
  OutPDF += "DIJETS_"
if MC15c:
  OutPDF += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
if HLLHC_pesimistic:
  OutPDF += "HLLHC_pesimistic"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
OutPDF += "_3"
Plot3 = Plotter1D(OutPDF) # Name of the output file
Plot3.ApplyFormat(Format)
Plot3.SetLegendPosition("TopLeft")  # Other options: Top, TopLeft, Bottom
if MC15c:
  Plot3.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
else:
  Plot3.ShowCME("14")    # Show 13 TeV
#Plot2.AddText(0.2,0.765,Text) # x, y, text
Plot3.AddText(0.35,0.82,Text) # x, y, text
counter = 0
h_JER = InputFile.Get("JET_JER_SINGLE_NP__1up")
h_JER_clone = h_JER.Clone("h_JER")
for syst in Systs_3:
  Legend = TString(syst)
  Legend.ReplaceAll("__1up","")
  Legend.ReplaceAll("JET_","")
  Plot3.AddHist(str(counter),File,syst) # ID,TFile,histName
  Plot3.SetLegend(str(counter),Legend) # ID,TFile,histName
  Plot3.SetColor(str(counter),Colors[counter])
  if not counter % 2 == 0:
    Plot3.NoTLegend(str(counter))
  if syst is "JET_JER_SINGLE_NP__1up":
    #Plot3.AddHist("JER_down",File,syst) # ID,TFile,histName
    Plot3.AddHist("JER_down",h_JER_clone) # ID,TFile,histName
    Plot3.SetColor("JER_down",Colors[counter])
    Plot3.Normalize("JER_down","Factor",-1.)
    Plot2.Scale("JER_down",-1.)
    Plot3.NoTLegend("JER_down")
  counter += 1
Plot3.AddHist("Total",hTotal_up)
Plot3.AddHist("TotalDown",hTotal_down,)
Plot3.SetColor("Total",kBlack)
Plot3.SetColor("TotalDown",kBlack)
Plot3.NoTLegend("TotalDown")
if IJXS:
  Plot3.SetRange("Y",-0.15,0.3)
Plot3.Write()  # Saves plot into "pT.pdf"



InputFile.Close()
print ">>>> DONE <<<<"


