#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

File = "/home/jbossios/cern/HLLHC/Inputs/TriggerStudies/v14/Output.root"

Bins    = ["00_05","05_10","10_15","15_20","20_25","25_30"]
pTBins  = ["35","45","55","70","85","100","116","134","152","172","194","216","240","264","290","318","346"]
etaBins = [0+0.1*etabin for etabin in range(0,46)] 

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Load easyPlot
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")

# Plotter Instance for fomatting
Format = Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
#Format.SetRange("Y",10E-26,10E10)
#Format.SetAxisTitles("#it{p}_{T} [GeV]","d^{2}\sigma/d#it{p}_{T}dy [pb/GeV]") # x-axis, y-axis
Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
Format.SetLineWidthAll(2)
Format.SetDrawOptionAll("HIST][")
Format.RebinAll(2)
#Format.SetTLegendPosition(0.66,0.67,0.93,0.92)

# pT plot
OutPDF = "../Plots/TriggerStudies/pT"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetLogx()
Plot.SetLogy()
Plot.SetMoreLogLabelsX()
#Plot.SetRange("X",300,1000000)
Plot.AddHist("pT",File,"pT","P")
Plot.NoTLegend("pT")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("#it{p}_{T} [GeV]","Events") # x-axis, y-axis
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# pT plot
OutPDF = "../Plots/TriggerStudies/pT_HSvsPU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetLogx()
Plot.SetLogy()
Plot.SetMoreLogLabelsX()
Plot.AddHist("All jets",File,"pT","P")
Plot.AddHist("HS jets",File,"pT_HS","P")
Plot.AddHist("PU jets",File,"pT_PU","P")
Plot.NoTLegend("pT")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("#it{p}_{T} [GeV]","Events") # x-axis, y-axis
Plot.Write()  # Saves plot into OutPDF

# pT plot
OutPDF = "../Plots/TriggerStudies/FractionPUjets"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetComparisonType("RatioOnly")   # Will show second panel with ratio
Plot.SetLogx()
Plot.SetLogy()
Plot.SetMoreLogLabelsX()
Plot.AddHist("All jets",File,"pT","P")
Plot.AddHist("HS jets",File,"pT_HS","P")
Plot.AddHist("PU jets",File,"pT_PU","P")
Plot.NoTLegend("pT")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("#it{p}_{T} [GeV]","Fraction") # x-axis, y-axis
Plot.Write()  # Saves plot into OutPDF



# Timing plot
OutPDF = "../Plots/TriggerStudies/Timing"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",-40,40)
Plot.AddHist("All",File,"Timing")
Plot.AddHist("HS jets",File,"Timing_HS")
Plot.AddHist("PU jets",File,"Timing_PU")
Plot.SetAxisTitles("Timing","Events") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# nConstituent plot
OutPDF = "../Plots/TriggerStudies/nConstituent"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",0,45)
Plot.AddHist("nConstit",File,"nConstit")
Plot.NoTLegend("nConstit")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
Plot.RebinAll(4)
#Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

for pTbin in range(0,len(pTBins)-1):
  OutPDF = "../Plots/TriggerStudies/nConstituent_HSvsPU_"+pTBins[pTbin]
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",0,45)
  Plot.AddHist("HS jets",File,"nConstit_HS_pt_"+pTBins[pTbin])
  Plot.AddHist("PU jets",File,"nConstit_PU_pt_"+pTBins[pTbin])
  Plot.NoTLegend("nConstit")
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
  Plot.RebinAll(4)
#  Plot.RebinAll(2)
  Plot.AddText(0.25,0.89,pTBins[pTbin]+"<#it{p}_{T}[GeV]<"+pTBins[pTbin+1])
  Plot.Write()  # Saves plot into OutPDF

for pTbin in range(0,len(pTBins)-1):
  OutPDF = "../Plots/TriggerStudies/nConstituent_leadingjets_HSvsPU_"+pTBins[pTbin]
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",0,45)
  Plot.AddHist("Leading HS jets",File,"nConstit_leadingHS_pt_"+pTBins[pTbin])
  Plot.AddHist("Leading PU jets",File,"nConstit_leadingPU_pt_"+pTBins[pTbin])
  Plot.NoTLegend("nConstit")
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
  Plot.RebinAll(4)

#  Plot.RebinAll(2)
  Plot.AddText(0.25,0.89,pTBins[pTbin]+"<#it{p}_{T}[GeV]<"+pTBins[pTbin+1])
  Plot.Write()  # Saves plot into OutPDF

for pTbin in range(0,len(pTBins)-1):
  OutPDF = "../Plots/TriggerStudies/nConstituent_secondleadingjets_HSvsPU_"+pTBins[pTbin]
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",0,45)
  Plot.AddHist("Second leading HS jets",File,"nConstit_secondHS_pt_"+pTBins[pTbin])
  Plot.AddHist("Second leading PU jets",File,"nConstit_secondPU_pt_"+pTBins[pTbin])
  Plot.NoTLegend("nConstit")
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
  Plot.RebinAll(4)
#  Plot.RebinAll(2)
  Plot.AddText(0.25,0.89,pTBins[pTbin]+"<#it{p}_{T}[GeV]<"+pTBins[pTbin+1])
  Plot.Write()  # Saves plot into OutPDF

for pTbin in range(0,len(pTBins)-1):
  OutPDF = "../Plots/TriggerStudies/nConstituent_HS_isolated_vs_nonisolated_"+pTBins[pTbin]
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",0,45)
  Plot.AddHist("Isolated HS jets",File,"nConstit_isolatedHS_pt_"+pTBins[pTbin])
  Plot.AddHist("Non-isolated HS jets",File,"nConstit_nonisolatedHS_pt_"+pTBins[pTbin])
  Plot.NoTLegend("nConstit")
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
  Plot.RebinAll(4)
#  Plot.RebinAll(2)
  Plot.AddText(0.25,0.89,pTBins[pTbin]+"<#it{p}_{T}[GeV]<"+pTBins[pTbin+1])
  Plot.Write()  # Saves plot into OutPDF

for pTbin in range(0,len(pTBins)-1):
  OutPDF = "../Plots/TriggerStudies/nConstituent_PU_isolated_vs_nonisolated_"+pTBins[pTbin]
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",0,45)
  Plot.AddHist("Isolated PU jets",File,"nConstit_isolatedPU_pt_"+pTBins[pTbin])
  Plot.AddHist("Non-isolated PU jets",File,"nConstit_nonisolatedPU_pt_"+pTBins[pTbin])
  Plot.NoTLegend("nConstit")
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
  Plot.RebinAll(4)
#  Plot.RebinAll(2)
  Plot.AddText(0.25,0.89,pTBins[pTbin]+"<#it{p}_{T}[GeV]<"+pTBins[pTbin+1])
  Plot.Write()  # Saves plot into OutPDF


# mu plot
OutPDF = "../Plots/TriggerStudies/mu"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",180,220)
Plot.AddHist("mu",File,"mu")
Plot.NoTLegend("mu")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("actualInteractionsPerCrossing","Events") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# timing vs mu plot
nMuBins = 8
muBins  = [180,185,190,195,200,205,210,215,220]
# get histogram
iFile   = TFile.Open(File)
Hist    = iFile.Get("h_timing_vs_mu")
Profile = Hist.ProfileY("timing_vs_mu_profile")
#for imu in range(0,nMuBins):
#  name  = "timing_vs_mu_"
#  name += str(imu)
#  low  = muHist.GetXaxis().FindBin(muBins[imu])
#  high = muHist.GetXaxis().FindBin(muBins[imu+1])-1
#  projection = muHist.ProjectionY(name,low,high,"e")
OutPDF = "../Plots/TriggerStudies/timing_vs_mu"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",180,220)
Plot.SetRange("Y",190,220)
Plot.AddHist("Profile",Profile)
Plot.NoTLegend("Profile")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("actualInteractionsPerCrossing","<Timing>") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

ResponseFile = "/home/jbossios/cern/HLLHC/Inputs/TriggerStudies/v2/Resolution_vs_pTtruth.root"

for etabin in range(0,45):
  name    = "Response_vs_pTtruth_Eta_"
  name   += str(etabin)
  OutPDF  = "../Plots/TriggerStudies/Response_vs_pTtruth_Eta_"
  OutPDF += str(etabin)
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.AddHist(name,ResponseFile,name,"P")
  Plot.NoTLegend(name)
  Plot.SetRange("X",0.,500.)
  Plot.SetLogx()
  Plot.SetMoreLogLabelsX()
  Plot.PlotLine("Y",1)
  Plot.RebinAll(1)
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("#it{p}_{T}^{truth} [GeV]","Response") # x-axis, y-axis
  Plot.Write()  # Saves plot into OutPDF

for etabin in range(0,45):
  name    = "Response_vs_nconstit_Eta_"
  name   += str(etabin)
  OutPDF  = "../Plots/TriggerStudies/Response_vs_nconstit_Eta_"
  OutPDF += str(etabin)
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.AddHist(name,ResponseFile,name,"P")
  Plot.NoTLegend(name)
  Plot.SetRange("X",0.,40.)
  Plot.SetRange("Y",0.9,1.5)
  Plot.PlotLine("Y",1)
  Plot.AddText(0.2,0.9,str(etaBins[etabin])+"<|#eta|<"+str(etaBins[etabin+1]))
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("Number of clusters","Response") # x-axis, y-axis
  Plot.RebinAll(1)
  Plot.Write()  # Saves plot into OutPDF


# Cluster time
OutPDF = "../Plots/TriggerStudies/ClusterTime_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",-50,50)
Plot.AddHist("HS jets",File,"h_HS_cluster_time")
Plot.AddHist("PU jets",File,"h_PU_cluster_time")
Plot.SetAxisTitles("Time","") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Cluster time
#OutPDF = "../Plots/TriggerStudies/clusterTime_cleaning"
#Plot = Plotter1D(OutPDF) # Name of the output file
#Plot.ApplyFormat(Format)
#Plot.SetRange("X",-50,50)
#Plot.AddHist("Pass cleaning",File,"h_HS_passLooseBad_clusterTime")
#Plot.AddHist("Not pass cleaning",File,"h_HS_notpassLooseBad_clusterTime")
#Plot.SetAxisTitles("Time","") # x-axis, y-axis
#Plot.NormalizeAll("Integral")
#Plot.Write()  # Saves plot into OutPDF


# Cluster time
OutPDF = "../Plots/TriggerStudies/ClusterEnergy_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",-40,40)
Plot.SetLogx()
Plot.SetMoreLogLabelsX()
Plot.AddHist("HS jets",File,"h_HS_cluster_e")
Plot.AddHist("PU jets",File,"h_PU_cluster_e")
Plot.SetAxisTitles("Energy [GeV]","") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Cluster time
OutPDF = "../Plots/TriggerStudies/NumberNegativeEclusters_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",-40,40)
Plot.SetLogy()
#Plot.SetMoreLogLabelsX()
Plot.AddHist("HS jets",File,"h_HS_negativeEclusters")
Plot.AddHist("PU jets",File,"h_PU_negativeEclusters")
Plot.SetAxisTitles("Number of clusters","") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Cluster time
OutPDF = "../Plots/TriggerStudies/dRNegativeEclusters_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",-40,40)
#Plot.SetLogx()
#Plot.SetMoreLogLabelsX()
Plot.AddHist("HS jets",File,"h_HS_negativeEclusters_dr")
Plot.AddHist("PU jets",File,"h_PU_negativeEclusters_dr")
Plot.SetAxisTitles("#Delta R(cluster,jet)","") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF


# Cluster energy fraction
OutPDF = "../Plots/TriggerStudies/clusterEfrac_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",-40,40)
#Plot.SetLogx()
#Plot.SetMoreLogLabelsX()
Plot.AddHist("HS jets",File,"h_HS_cluster_efrac")
Plot.AddHist("PU jets",File,"h_PU_cluster_efrac")
Plot.SetAxisTitles("cluster_e/jetE","") # x-axis, y-axis
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Cluster time
OutPDF = "../Plots/TriggerStudies/ClusterTime_GoodTile_vs_BadTile"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",-50,50)
Plot.AddHist("TILEQ=0",File,"h_GoodTile_ClusterTime")
Plot.AddHist("TILEQ>0",File,"h_BadTile_ClusterTime")
Plot.SetAxisTitles("Time","") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Cluster energy fraction
OutPDF = "../Plots/TriggerStudies/clusterEfrac_GoodTile_vs_BadTile"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",-40,40)
#Plot.SetLogx()
#Plot.SetMoreLogLabelsX()
Plot.AddHist("TILEQ=0",File,"h_GoodTile_eFrac")
Plot.AddHist("TILEQ>0",File,"h_BadTile_eFrac")
Plot.SetAxisTitles("cluster_e/jetE","") # x-axis, y-axis
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# nConstituent plot
OutPDF = "../Plots/TriggerStudies/nConstituent_GoodTile_vs_BadTile"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",0,45)
Plot.AddHist("TILEQ=0",File,"h_GoodTile_nConstit")
Plot.AddHist("TILEQ>0",File,"h_BadTile_nConstit")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("Number of clusters","") # x-axis, y-axis
#Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF


OutPDF = "../Plots/TriggerStudies/jetShape_GoodTile_vs_BadTile"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",0,45)
Plot.AddHist("TILEQ=0",File,"h_GoodTile_jetShape")
Plot.AddHist("TILEQ>0",File,"h_BadTile_jetShape")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("clusterE #times dR / jetE","") # x-axis, y-axis
#Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

OutPDF = "../Plots/TriggerStudies/clusterShape_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",0,45)
Plot.AddHist("HS jets",File,"h_HS_clusterShape")
Plot.AddHist("PU jets",File,"h_PU_clusterShape")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("clusterE #times dR / jetE","") # x-axis, y-axis
#Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

OutPDF = "../Plots/TriggerStudies/FracSamplingMax_HS_vs_PU"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",0,45)
Plot.AddHist("HS jets",File,"h_HS_fracSamplingMax")
Plot.AddHist("PU jets",File,"h_PU_fracSamplingMax")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("FracSamplingMax","") # x-axis, y-axis
#Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

for ieta in range(0,6):
  OutPDF = "../Plots/TriggerStudies/FracSamplingMax_HS_vs_PU_eta_"+str(ieta)
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  #Plot.SetRange("X",0,45)
  Plot.AddHist("HS jets",File,"h_HS_fracSamplingMax_eta_"+str(ieta))
  Plot.AddHist("PU jets",File,"h_PU_fracSamplingMax_eta_"+str(ieta))
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.SetAxisTitles("FracSamplingMax","") # x-axis, y-axis
  #Plot.RebinAll(2)
  #Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  #Plot.SetLegend(str(counter),IJXSLegends[counter])
  Plot.Write()  # Saves plot into OutPDF

  # Cluster time
  OutPDF = "../Plots/TriggerStudies/ClusterTime_HS_vs_PU_eta_"+str(ieta)
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.SetRange("X",-50,50)
  Plot.AddHist("HS jets",File,"h_HS_cluster_time_eta_"+str(ieta))
  Plot.AddHist("PU jets",File,"h_PU_cluster_time_eta_"+str(ieta))
  Plot.SetAxisTitles("Time","") # x-axis, y-axis
  Plot.Write()  # Saves plot into OutPDF

for isample in range(0,27):
  OutPDF = "../Plots/TriggerStudies/clusterEfrac_HS_vs_PU_sample_"+str(isample)
  Plot = Plotter1D(OutPDF) # Name of the output file
  Plot.ApplyFormat(Format)
  Plot.AddHist("HS jets",File,"h_HS_eFrac_sample_"+str(isample))
  Plot.AddHist("PU jets",File,"h_PU_eFrac_sample_"+str(isample))
  Plot.SetAxisTitles("cluster_e/jetE","") # x-axis, y-axis
  Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
  Plot.Write()  # Saves plot into OutPDF

print ">>>> DONE <<<<"


